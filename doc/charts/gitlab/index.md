# GitLab components sub-charts

The following GitLab component services are used:

- GitLab/[sidekiq](sidekiq/index.md)
- GitLab/[gitlab-shell](gitlab-shell/index.md)
- GitLab/[gitaly](gitaly/index.md)
- GitLab/[unicorn](unicorn/index.md)
- GitLab/[migrations](migrations/index.md)
